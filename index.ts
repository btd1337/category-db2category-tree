import { Categoria } from "./categoria";
// import * as readline from "readline";
import { CategoriaDb } from "./categoria-db.entity";

/* const _readline = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let value: string;

let categoria = criaArvore();

gerarArvoreFalsa();

*/

function criaArvore(): Categoria {
  let _categorias = {
    id: "0",
    nome: "Categorias",
    subCategorias: new Array<Categoria>(),
  };

  return _categorias;
}

function insereNo(categoriaDb: CategoriaDb, categorias: Categoria): void {
  // Não aponta para um pai
  if (!categoriaDb.pai) {
    insereNoAux(categoriaDb, categorias);
  } else {
    // Aponta para um pai
    // Checa se o pai já está registrado
    let noPai = buscaNo(categoriaDb.pai, categorias);

    // Pai não registrado
    if (!noPai) {
      // registra pai na raiz
      insereNoAux(
        {
          id: categoriaDb.pai,
          nome: null,
          pai: null,
        },
        categorias,
        true
      );

      noPai = categorias.subCategorias.find(
        (element) => element.id === categoriaDb.pai
      );
      insereNoAux(categoriaDb, noPai, true);
    } else {
      // pai já registrado
      insereNoAux(categoriaDb, noPai);
    }
  }
}

// isChecked: Se nó já foi procurado antes
// não procurar novamente
function insereNoAux(
  categoriaDb: CategoriaDb,
  noPai: Categoria,
  isChecked: boolean = false
): void {
  // verifica se nó já foi registrado antes como pai de outro nó
  if (!isChecked) {
    let no = noPai.subCategorias.find(
      (element) => element.id === categoriaDb.id
    );
    if (no && no.nome === null) {
      no.nome = categoriaDb.nome;
    } else {
      noPai.subCategorias.push(Categoria.fromCategoriaDb(categoriaDb));
    }
  } else {
    noPai.subCategorias.push(Categoria.fromCategoriaDb(categoriaDb));
  }

  noPai.subCategorias.sort((a, b) =>
    a.nome > b.nome ? 1 : b.nome > a.nome ? -1 : 0
  );
}

function obterCategoriasDasCategoriasDb(
  categoriasDb: CategoriaDb[]
): Categoria {
  let categorias: Categoria = criaArvore();

  categoriasDb.forEach((element) => {
    insereNo(element, categorias);
  });

  return categorias;
}

function buscaNo(valorNo: string, categoria: Categoria) {
  if (categoria.id === valorNo) {
    return categoria;
  }

  if (categoria.subCategorias.length === 0) {
    return null;
  }

  let isCategoriaEncontrada: Categoria;

  for (let element of categoria.subCategorias) {
    isCategoriaEncontrada = buscaNo(valorNo, element);

    if (isCategoriaEncontrada) {
      return isCategoriaEncontrada;
    }
  }
}

function gerarArvoreFalsa(categoria: Categoria) {
  categoria.subCategorias.push({
    id: "1",
    nome: "Ensino Primário",
    subCategorias: new Array<Categoria>(),
  });

  categoria.subCategorias.push({
    id: "2",
    nome: "Ensino Médio",
    subCategorias: new Array<Categoria>(),
  });

  categoria.subCategorias.push({
    id: "3",
    nome: "Ensino Superior",
    subCategorias: new Array<Categoria>(),
  });

  categoria.subCategorias[0].subCategorias.push({
    id: "4",
    nome: "Letras",
    subCategorias: new Array<Categoria>(),
  });
}

function Main() {
  const categoriasDb: CategoriaDb[] = [
    {
      id: "1",
      nome: "Ensino Primário",
      pai: null,
    },
    {
      id: "8",
      nome: "Português",
      pai: "3",
    },
    {
      id: "2",
      nome: "Ensino Fundamental",
      pai: null,
    },
    {
      id: "3",
      nome: "Ensino Médio",
      pai: null,
    },
    {
      id: "4",
      nome: "Ensino Superior",
      pai: null,
    },
    {
      id: "5",
      nome: "Ensino Pós-Superior",
      pai: null,
    },
    {
      id: "6",
      nome: "Linguas",
      pai: "2",
    },
    {
      id: "7",
      nome: "Inglês",
      pai: "6",
    },
  ];

  const categorias: Categoria = obterCategoriasDasCategoriasDb(categoriasDb);

  /* 
  _readline.question("Valor a buscar: ", (_value) => {
  value = _value;
  console.log("Busca", buscaNo(value, categoria));
  _readline.close();
  }); */

  console.log("Categorias", categorias);
}

Main();
