import { CategoriaDb } from "./categoria-db.entity";

export class Categoria {
  id: string;
  nome: string;
  subCategorias: Categoria[];

  static fromCategoriaDb(categoriaDb: CategoriaDb) {
    const categoria = new Categoria();
    categoria.id = categoriaDb.id;
    categoria.nome = categoriaDb.nome;
    categoria.subCategorias = new Array<Categoria>();

    return categoria;
  }
}
